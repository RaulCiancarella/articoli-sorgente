{
 {{-((2*Y20[\[Theta][], \[Phi][]]*(8*E^(2*\[CapitalLambda][r[]])*Pi*r[]^2*
          \[Delta]\[Epsilon][r[]] + E^(2*\[CapitalLambda][r[]])*
          \[CapitalKappa][r[]] - r[]*Derivative[1][\[CapitalEta]][r[]] + 
         3*r[]*Derivative[1][\[CapitalKappa]][r[]] - 
         r[]^2*Derivative[1][\[CapitalKappa]][r[]]*
          Derivative[1][\[CapitalLambda]][r[]] + \[CapitalEta][r[]]*
          (-1 + 2*r[]*Derivative[1][\[CapitalLambda]][r[]]) + 
         r[]^2*Derivative[2][\[CapitalKappa]][r[]]))/
       E^(2*\[CapitalLambda][r[]]) + (\[CapitalEta][r[]] + 
        \[CapitalKappa][r[]])*(Csc[\[Theta][]]^2*Derivative[0, 2][Y20][
          \[Theta][], \[Phi][]] + Cot[\[Theta][]]*Derivative[1, 0][Y20][
          \[Theta][], \[Phi][]] + Derivative[2, 0][Y20][\[Theta][], 
         \[Phi][]]))/(2*r[]^2), 0, 0, 0}, 
  {0, ((-2*Y20[\[Theta][], \[Phi][]]*(8*E^(2*\[CapitalLambda][r[]])*Pi*r[]^2*
         \[Delta]p[r[]] + E^(2*\[CapitalLambda][r[]])*\[CapitalKappa][r[]] - 
        r[]*Derivative[1][\[CapitalEta]][r[]] + 
        r[]*Derivative[1][\[CapitalKappa]][r[]] + 
        r[]^2*Derivative[1][\[CapitalKappa]][r[]]*
         Derivative[1][\[CapitalPhi]][r[]] - \[CapitalEta][r[]]*
         (1 + 2*r[]*Derivative[1][\[CapitalPhi]][r[]])))/
      E^(2*\[CapitalLambda][r[]]) + (\[CapitalEta][r[]] - 
       \[CapitalKappa][r[]])*(Csc[\[Theta][]]^2*Derivative[0, 2][Y20][
         \[Theta][], \[Phi][]] + Cot[\[Theta][]]*Derivative[1, 0][Y20][
         \[Theta][], \[Phi][]] + Derivative[2, 0][Y20][\[Theta][], 
        \[Phi][]]))/(2*r[]^2), 
   -((Derivative[1][\[CapitalEta]][r[]] - Derivative[1][\[CapitalKappa]][
        r[]] + 2*\[CapitalEta][r[]]*Derivative[1][\[CapitalPhi]][r[]])*
      Derivative[1, 0][Y20][\[Theta][], \[Phi][]])/
    (2*E^(2*\[CapitalLambda][r[]])), 
   -((Derivative[1][\[CapitalEta]][r[]] - Derivative[1][\[CapitalKappa]][
        r[]] + 2*\[CapitalEta][r[]]*Derivative[1][\[CapitalPhi]][r[]])*
      Derivative[0, 1][Y20][\[Theta][], \[Phi][]])/
    (2*E^(2*\[CapitalLambda][r[]]))}, 
  {0, -((Derivative[1][\[CapitalEta]][r[]] - Derivative[1][\[CapitalKappa]][
        r[]] + 2*\[CapitalEta][r[]]*Derivative[1][\[CapitalPhi]][r[]])*
      Derivative[1, 0][Y20][\[Theta][], \[Phi][]])/(2*r[]^2), 
   -(Y20[\[Theta][], \[Phi][]]*(16*E^(2*\[CapitalLambda][r[]])*Pi*r[]*
        \[Delta]p[r[]] + 2*Derivative[1][\[CapitalKappa]][r[]] + 
       2*\[CapitalEta][r[]]*Derivative[1][\[CapitalLambda]][r[]] - 
       r[]*Derivative[1][\[CapitalKappa]][r[]]*
        Derivative[1][\[CapitalLambda]][r[]] - 2*\[CapitalEta][r[]]*
        Derivative[1][\[CapitalPhi]][r[]] + 
       r[]*Derivative[1][\[CapitalKappa]][r[]]*Derivative[1][\[CapitalPhi]][
         r[]] + 2*r[]*\[CapitalEta][r[]]*Derivative[1][\[CapitalLambda]][r[]]*
        Derivative[1][\[CapitalPhi]][r[]] - 2*r[]*\[CapitalEta][r[]]*
        Derivative[1][\[CapitalPhi]][r[]]^2 + 
       Derivative[1][\[CapitalEta]][r[]]*
        (-2 + r[]*Derivative[1][\[CapitalLambda]][r[]] - 
         3*r[]*Derivative[1][\[CapitalPhi]][r[]]) - 
       r[]*Derivative[2][\[CapitalEta]][r[]] + 
       r[]*Derivative[2][\[CapitalKappa]][r[]] - 2*r[]*\[CapitalEta][r[]]*
        Derivative[2][\[CapitalPhi]][r[]]))/(2*E^(2*\[CapitalLambda][r[]])*
     r[]), 0}, {0, -(Csc[\[Theta][]]^2*(Derivative[1][\[CapitalEta]][r[]] - 
       Derivative[1][\[CapitalKappa]][r[]] + 2*\[CapitalEta][r[]]*
        Derivative[1][\[CapitalPhi]][r[]])*Derivative[0, 1][Y20][\[Theta][], 
       \[Phi][]])/(2*r[]^2), 0, 
   -(Y20[\[Theta][], \[Phi][]]*(16*E^(2*\[CapitalLambda][r[]])*Pi*r[]*
        \[Delta]p[r[]] + 2*Derivative[1][\[CapitalKappa]][r[]] + 
       2*\[CapitalEta][r[]]*Derivative[1][\[CapitalLambda]][r[]] - 
       r[]*Derivative[1][\[CapitalKappa]][r[]]*
        Derivative[1][\[CapitalLambda]][r[]] - 2*\[CapitalEta][r[]]*
        Derivative[1][\[CapitalPhi]][r[]] + 
       r[]*Derivative[1][\[CapitalKappa]][r[]]*Derivative[1][\[CapitalPhi]][
         r[]] + 2*r[]*\[CapitalEta][r[]]*Derivative[1][\[CapitalLambda]][r[]]*
        Derivative[1][\[CapitalPhi]][r[]] - 2*r[]*\[CapitalEta][r[]]*
        Derivative[1][\[CapitalPhi]][r[]]^2 + 
       Derivative[1][\[CapitalEta]][r[]]*
        (-2 + r[]*Derivative[1][\[CapitalLambda]][r[]] - 
         3*r[]*Derivative[1][\[CapitalPhi]][r[]]) - 
       r[]*Derivative[2][\[CapitalEta]][r[]] + 
       r[]*Derivative[2][\[CapitalKappa]][r[]] - 2*r[]*\[CapitalEta][r[]]*
        Derivative[2][\[CapitalPhi]][r[]]))/(2*E^(2*\[CapitalLambda][r[]])*
     r[])}}}

\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}First step}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}A Powerful Mathematica Tools}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}ODE for H}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Non diagonal component}{4}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Diagonal component}{4}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Background constraint}{4}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Form H(r) to $\lambda $ }{5}{subsection.2.2.4}

# Builds and compiles Build_Star according to the parameters

head -n48 Build_Star.f >> prog1.f
echo "      parameter (n_eos="$neos")" >> prog1.f
echo "      parameter (gc2="$gc2")" >> prog1.f
echo "      parameter (cc="$cc")" >> prog1.f
echo "      parameter (MSun="$MSun")" >> prog1.f

if [ "$NSswitch" -eq 1 ]; then
    echo "      data switch/.true./" >> prog1.f
else
    echo "      data switch/.false./" >> prog1.f
fi
if [ "$MOMswitch" -eq 1 ]; then
    echo "      data mswitch/.true./" >> prog1.f
else
    echo "      data mswitch/.false./" >> prog1.f
fi
head -n90 Build_Star.f | tail -n35 >> prog1.f
echo "      enne = "$enne >> prog1.f

if [ "$EOSswitch" -eq 0 ]; then
    tail -n319 Build_Star.f >> prog1.f
    if [ $enne == "0d0" ]; then
	head -n3 eos.f >> prog2.f
	tail -n102 eos.f | head -n9 >> prog2.f
	if [ -f EOS_TABLES/$eosname.eos ]; then
	    eoslines="$(wc -l EOS_TABLES/$eosname.eos | cut -d E -f 1)"
	    echo "      parameter (n="$eoslines")" >> prog2.f
	else
	    echo "ERROR: EOS NAME NOT RECOGNIZED"
	    echo "       USING APR2"
	    eosname="apr2"
	    eoslines="$(wc -l EOS_TABLES/$eosname.eos | cut -d E -f 1)"
	    echo "      parameter (n="$eoslines")" >> prog2.f
	fi
	tail -n92 eos.f | head -n4 >> prog2.f
	echo "      parameter (gc2="$gc2")" >> prog2.f
	echo "      parameter (GG="$GG")" >> prog2.f
	echo "      parameter (cc="$cc")" >> prog2.f
	tail -n85 eos.f | head -n13 >> prog2.f
	echo "      open(unit=1,file='EOS_TABLES/"$eosname".eos')" >> prog2.f
	tail -n71 eos.f >> prog2.f
    else
        eosname=poly
	head -n21 eos.f >> prog2.f
    fi
else
    echo "      kn = 0d0" >> prog1.f
    tail -n315 Build_Star.f >> prog1.f
    head -n10 piecewise_eos.f >> prog2.f
    echo "      parameter (gc2="$gc2")" >> prog2.f
    echo "      parameter (cc="$cc")" >> prog2.f
    head -n17 piecewise_eos.f | tail -n5 >> prog2.f
    if [  "$EOSswitch" -eq 1 ]; then
	echo "      rho1 = 0.7033d14*gc2*1d10" >> prog2.f # 2H
	eosname=piecepoly2H
    elif [  "$EOSswitch" -eq 2 ]; then
	echo "      rho1 = 0.9308d14*gc2*1d10" >> prog2.f # 1.5H
	eosname=piecepoly2H
    elif [  "$EOSswitch" -eq 3 ]; then
	echo "      rho1 = 1.232d14*gc2*1d10" >> prog2.f  # H
	eosname=piecepolyH
    elif [  "$EOSswitch" -eq 4 ]; then
	echo "      rho1 = 1.417d14*gc2*1d10" >> prog2.f  # HB
	eosname=piecepolyHB
    elif [  "$EOSswitch" -eq 5 ]; then
	echo "      rho1 = 1.069d14*gc2*1d10" >> prog2.f  # HBs
	eosname=piecepolyHBs
    elif [  "$EOSswitch" -eq 6 ]; then
	echo "      rho1 = 0.6854d14*gc2*1d10" >> prog2.f # HBss
	eosname=piecepolyHBss
    elif [  "$EOSswitch" -eq 7 ]; then
	echo "      rho1 = 1.630d14*gc2*1d10" >> prog2.f  # B
	eosname=piecepolyB
    elif [  "$EOSswitch" -eq 8 ]; then
	echo "      rho1 = 1.269d14*gc2*1d10" >> prog2.f  # Bs
	eosname=piecepolyBs
    elif [  "$EOSswitch" -eq 9 ]; then
	echo "      rho1 = 0.8547d14*gc2*1d10" >> prog2.f # Bss
	eosname=piecepolyBss
    fi
    tail -n19 piecewise_eos.f >> prog2.f
fi
mv eos.f eos_bkup.f
mv piecewise_eos.f piecewise_eos_bkup.f
mv prog2.f eos.f
g77 prog1.f -o Build_Star
if [ -f Build_Star ]; then
    dummy='ok'
else
    echo
    echo
    echo "*** ERROR: Build_Star not compiling ***"
    echo
    echo
    exit -1
fi
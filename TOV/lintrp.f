c*******************************************
c SUBROUTINE FOR LOGARITHMIC INTERPOLATION *
c*******************************************
c
c LOGARTIHMIC INTERPOLATION USED TO GET P(RHO) OR RHO(P) AS NEEDED:
c
c     LOG P - LOG P(J)       LOG P(J+1) - LOG P(J)
c   -------------------- = -------------------------
c   LOG RHO - LOG RHO(J)   LOG RHO(J+1) - LOG RHO(J)
c
c Interpolazione logaritmica lineare per ottenere  p(rho) (se ntype = 1)
c o rho,(p) (se ntype = 2). lrhot e lpt sono vettori a nt componenti
c contenenti i logaritmi della densita' di massa energia e della
c pressione.
c------------------------------------------------------------------------
      SUBROUTINE LINTRP(RHO,P,LRHOT,LPT,NT,NTYPE) 
      IMPLICIT REAL*8 (A-M,O-Z) 
      DIMENSION LRHOT(NT),LPT(NT) 
      GO TO (10,20),NTYPE 
   10 LRHO=LOG(RHO) 
      DO 12 NP=1,NT 
      IF (LRHO.LE.LRHOT(NP)) GO TO 15 
   12 CONTINUE 
      WRITE(6,6661)
 6661 FORMAT(//,' *** INTERPOLATION FOR P(RHO) OUTSIDE OF RANGE ',//)
   15 N=NP-1 
      LP=LPT(N)+(LRHO-LRHOT(N))*(LPT(NP)-LPT(N))/(LRHOT(NP)-LRHOT(N)) 
      P=EXP(LP) 
      RETURN 
   20 LP=LOG(P) 
      DO 22 NP=1,NT 
      IF (LP.LE.LPT(NP)) GO TO 25 
   22 CONTINUE 
      WRITE(6,6662)
 6662 FORMAT(//,' *** INTERPOLATION FOR RHO(P) OUTSIDE OF RANGE ',//) 
   25 N=NP-1 
      LRHO=LRHOT(N)+(LP-LPT(N))*(LRHOT(NP)-LRHOT(N))/(LPT(NP)-LPT(N)) 
      RHO=EXP(LRHO) 
      RETURN 
      END 

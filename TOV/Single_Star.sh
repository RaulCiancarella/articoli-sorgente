#!/bin/bash

#Performs a run for each set of parameters specified by a line of par_file.dat
#Each line par_file.dat must contain the following parameters separated by a
#colon (:):
#  - Polytropic factor (K)
#  - NS central pressure (p0)
#  - Switch for self-gravity (NSswitch): 0 -> GR, 1 -> Newtonian gravity
#  - Scalar quadrupole moment switch (MOMswitch): 1 -> Newt, 0 -> corrected
#  - Number of points in star profile (neos)
#  - Polytropic index (enne): set to 0d0 for tabulated EOSs
#  - EOS file name (eosname)
#  - EOSswitch: 0 -> polytropic or tabulated EOS, else piecewise polytropic EOS

#G/c^2, speed of light and gravitational constant in cgs; solar mass in km
#gc2="7.424713284d-29"
#cc="2.99792458d10"
#GG="6.673d-8"
#MSun="1.47662504d0"
gc2="7.42365d-29"
GG="6.673d-8"
cc="2.998d10"
MSun="1.4768d0"

#Remove possible old executables and prog*.f files
rm -rf Build_Star
rm -rf prog1.f

#Check existence of par_file.dat. Then, if it does not exist yet,
#create a backup of the original par_file.dat; get number of lines in
#par_file.dat
if [ -f ../par_file.dat ]; then
    if [ -f ../par_file_bkup.dat ]; then
	n_runs="$(wc -l ../par_file.dat | cut -d . -f 1)"
    else
	cp ../par_file.dat ../par_file_bkup.dat
	n_runs="$(wc -l ../par_file.dat | cut -d . -f 1)"
    fi
else
    echo
    echo
    echo "*** ERROR: no par_file.dat in parent directory ***"
    echo
    echo
    rm -rf Build_Star
    exit -1
fi

while [ "$n_runs" -gt 0 ]
do
    #Read first line of parameter files
    pars="$(head -n1 ../par_file.dat)"

    #Recreate par_file.dat without the first line or delete it when done.
    n_runs="$(echo "$n_runs-1" | bc -l)"
    if [ "$n_runs" -gt 0 ]; then
	tail -n$n_runs ../par_file.dat > ../tmp.dat
	rm ../par_file.dat
	mv ../tmp.dat ../par_file.dat
    else
	rm ../par_file.dat
    fi
    
    #Establish the set of parameters from the string just read
    K="$(echo $pars | cut -d: -f 1)"          #Polytropic factor K
    p0="$(echo $pars | cut -d: -f 2)"         #NS central pressure
    NSswitch="$(echo $pars | cut -d: -f 3)"   #0 = GR, 1 = Newtonian gravity
    MOMswitch="$(echo $pars | cut -d: -f 4)"  #0 = corrected, 1 = Newtonian
    neos="$(echo $pars | cut -d: -f 5)"       #number of points in star profile
    enne="$(echo $pars | cut -d: -f 6)"       #Polytropic index
    eosname="$(echo $pars | cut -d: -f 7)"    #EOS name
    EOSswitch="$(echo $pars | cut -d: -f 8)"  #>0 = piecewise polytropic EOS

    source CompileStar.sh
    
    ./Build_Star $K $p0
    
    #Remove run leftovers
    mv eos.f eos_used.f
    mv eos_bkup.f eos.f
    mv piecewise_eos_bkup.f piecewise_eos.f
    rm -rf prog1.f
done

#Tidy up
rm -f Build_Star

exit

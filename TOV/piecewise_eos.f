c---------------------------------------------------------------------
      SUBROUTINE EOS(pres,rho,epsilon,depsdp)
c---------------------------------------------------------------------
c 2-piecewise polytropic EOS. Input and output: (Km^-2, Km^-2, Km^-2).
c log10Pfidu (K), gamma2(enne), and rho1 are inputs.
c---------------------------------------------------------------------
      implicit none
      real*8 gamma1, K1, rhofidu, rho1, gamma2, enne, K2, K
      real*8 pres, rho, epsilon, depsdp, cs2, gc2, cc
      common /adiabatic/ enne, K   ! enne is actually Gamma and K is log10p
      parameter(gc2=7.42365d-29)   ! cm/g
      parameter(cc=2.998d10)       ! cm/s
      
      gamma1 = 1.35692395d0        ! Crust EOS
      K1 = 3.99873692d-8*(gc2*1d10)**(1d0-gamma1)
      rhofidu = 10d0**14.7d0*gc2*1d10

      rho1 = 0.7033d14*gc2*1d10    ! Crust-core interface !FINE
      gamma2 = enne                ! Core EOS (the enne input is really gamma2)
      K2 = 10d0**K*gc2*1d10/rhofidu**gamma2 ! K is log10 of P at rho-fiducial

      if(pres.lt.K1*rho1**gamma1) then
         rho = (pres/K1)**(1.d0/gamma1)
         epsilon = rho + pres/(gamma1-1d0)
         cs2 = gamma1*pres/(epsilon+pres) ! (Kokkotas-Gaertig), careful: eps!
         depsdp = 1d0/cs2
!     dpdrho = K*gamma1*rho**(gamma1-1d0)
      else
         rho = (pres/K2)**(1.d0/gamma2)
         epsilon = rho + pres/(gamma2-1d0)
         cs2 = gamma2*pres/(epsilon+pres) ! (Kokkotas-Gaertig), careful: eps!
         depsdp = 1d0/cs2
!     dpdrho = K*gamma2*rho**(gamma2-1d0)
      endif
      
      return
      end

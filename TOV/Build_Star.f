c--------------------------------------------------------------------
      program Build_Star
c--------------------------------------------------------------------
c Variable meanings line by line:
c Pi=3.14... and G*c^2 (kmkgs), solar mass (km), light speed (km/s).
c Central density in km^-2 and in cgs, derivative in r=0 of the
c energy density wrt the pressure, central pressure, central energy
c density in km^-2 and in cgs.
c Y(1) = radial coordinate r of the Schwarzschild metric
c Y(2) = integral of rho in dV (Newtonian mass)
c Y(3) = nu of g00 = exp(nu)
c Y(4) = integral of eps in dV (TOV mass)
c Y(5) = Pseudo-TOV gravitational potential ***
c Y(6) = Pseudo-TOV self-gravity potential ***
c Y(7) = Scalar quadrupole moment
c Y(8) = Integral of rho*u^t*sqrt(-g) in dV (baryonic mass)
c Y(9) = (l=2,m=0) perturbation of a NS metric
c Y(10) = derivative of Y(9) wrt r
c and their derivatives wrt ln(p); integration limits for odeint,
c small number, odeint parameter.
c Polytropic index, kappa_n, nu(0) (with g00 = exp(nu)), PhiTOV(0).
c Z(1)=ln(P)(r), Z(2)=mass(r), sampling step for the profiles of
c P, rho and epsilon in r.
c Odeint variables, counter.
c Compactness, optimization variable, tidal Love number.
c Logic variables to choose between GR or Newtonian gravity, and
c between a classic scalar quadrupole moment and a corrected one. 
c Command line arguments.
c
c *** Passare da integrali in dV ad integrali in
c     dV/sqrt(1-2M_TOV(r)/r) migliora i risultati?
c---------------------------------------------------------------------
      implicit none
      real*8 pi, gc2, MSun, cc
      real*8 rho0, rhocent, depsdp0, p0, eps0, epscent
      real*8 y(10), dydx(10), x1, x2, h1, hh
      real*8 enne, kn, K, nu0, Phi0
      real*8 Z(2), dx
      integer nok, nbad, i, j, n_eos, count
      real*8 comp, yy, k2
      logical switch, mswitch
      character arg*80

      common /adiabatic/ enne, k
      common /NewtGR/ switch, mswitch

      EXTERNAL derivs_pseudotov, derivs_pseudotov_r

      parameter(n_eos=2000)
      parameter(gc2=7.42365d-29) ! da 1/km^2 a g/cm^3
      parameter(cc=2.998d10)     ! cm/s
      parameter(MSun=1.4768d0)
      data switch/.false./  !false = GR, true = Newtonian gravity
      data mswitch/.false./  !true = Newt momquad, false = corrected momquad

c      call system('clear')

      open(9, FILE='BuiltStar.dat')
      open(12, FILE='star.dat')
      open(13, FILE='profiles.dat')

      write(9,*) '****************************************************',
     & '***********************'
      if(iargc().ne.2) then
         print*, '* ERROR: I need 2 arguments!'
         print*, '*        1) The coefficient K of the n=1 polytrope'
         print*, '*        2) The central pressure'
         goto 222
      endif

 21   format(d21.15)
 22   format(1x,A,1x,d21.15)
 23   format(1x,A,1x,d21.15,1x,A)
 24   format(d21.15,1x,d21.15,1x,d21.15,1x,d21.15,1x,d21.15)
      pi = dacos(-1.d0)

c Useful values of p0 are given in p0.txt 
      call getarg(1,arg)
      read(arg,*) K
      call getarg(2,arg)
      read(arg,*) p0
      if(K.le.0d0) then
         print*, '* WARNING: INVALID K!'
         goto 222
      endif
      if(p0.le.0d0) then
         print*, '* WARNING: INVALID p0!'
         goto 222
      endif

      enne = 1d0
      call kappa(enne,kn)
      if(enne.ne.0d0) then
         p0 = p0/(K**enne)      ! Rescale p0 only for polytropes
      endif

      CALL EOS(p0,rho0,eps0,depsdp0)
!      print*, p0, rho0, eps0, depsdp0
c Calculating rho in physical units; IT IS NOT USED IN THE INTEGRATION 
      rhocent = rho0/gc2*1d-10
      epscent = eps0/gc2*1d-10

c*************************************************************************
c Integrate stellar struction equations                                  *
c -------------------------------------                                  *
c                                                                        *
c The integration is halted when the pressure is a factor 10^9 smaller   *
c than the central pressure.                                             *
c WARNING: the integration variable in DERIVS_* is the logarithm of the  *
c pressure and x1 and x2, the integration limits, are log(P0-h1) and     *
c log(10^{-9}P0), so that the first thing to do in DERIVS_* is to invert *
c the logarithm in oder to obtain P.                                     *
c*************************************************************************
      h1 = 1.d-09*P0
      x1 = DLOG(P0-h1)    ! The integration does not start at exactly r=0
      x2 = DLOG(1.d-9*P0) ! 6->9 makes GNH3 results coincide with Bulik's ones; use 5 with SQM
c      x2 = DLOG(2d29*gc2*1.d10/cc/cc)! For strange-quark stars 
      hh = 1.d-8*(x2-x1)  ! This d-8 prevents rkqs underflow

      if(switch) then
         Y(1) = DSQRT(1.5d0*h1/PI)/rho0 ! Newtonian Radius
      else
         Y(1) = DSQRT((1.5d0*h1/PI)/((eps0+P0)*(eps0+3.d0*P0))) ! TOV Radius
      endif
      Y(2) = 4d0/3d0*PI*rho0*Y(1)**3     ! Newtonian mass(r)
      Y(3) = 0d0                         ! nu(r)
      Y(4) = 4d0/3d0*PI*eps0*Y(1)**3     ! TOV mass(r)
      Y(5) = 0d0                         ! TOV/Newtonian Phi(r)
      Y(6) = 0d0                         ! TOV Omega(r)
      Y(7) = 0d0                         ! Scalar quadrupole moment(r)
      Y(8) = Y(2)                        ! Baryonic mass(r)
      Y(9) = Y(1)**2*(1d0-2d0*PI/7d0*(eps0/3d0+11d0*P0+
     &     (eps0+P0)*depsdp0)*Y(1)**2)   ! (l=2,m=0) perturbation (r)
      Y(10) = 2d0*Y(1)*(1d0-4d0*PI/7d0*(eps0/3d0+11d0*P0+
     &     (eps0+P0)*depsdp0)*Y(1)**2)   ! dy(9)/dr

      call odeint(y,10,x1,x2,hh,nok,nbad,derivs_pseudotov)

      nu0 = -Y(3) + DLOG(1d0-2d0*Y(4)/Y(1))   
      if(switch) then
         Phi0 = -Y(5) -Y(2)/Y(1) ! Test: Newtonian boundary condition
      else
         Phi0 = -Y(5) + DLOG(1d0-2d0*Y(4)/Y(1))/2d0 !TOV Boundary condition
      endif
      write(9,*)
      write(9,*) '*************************  NS EQUILIBRIUM MODEL  ***',
     & '***********************'
      if(switch) then
         write(9,23) '* M Newt=',y(2)/MSun, '[solar masses]'
      else
         write(9,23) '* M TOV =',y(4)/MSun, '[solar masses]'
      endif
      write(9,23) '* M Bar =',y(8)/MSun, '[solar masses]'
      write(9,23) '* R =',y(1), '[km]'
      if(switch) then
         write(9,22) '* M Newt/R =', Y(2)/Y(1)
      else
         write(9,22) '* M TOV/R =', Y(4)/Y(1)
      endif
      write(9,22) '* M Bar/R =', Y(8)/Y(1)
      write(9,*) '*'
      write(9,23) '* Central density =',rho0/1.4768d0/1.4768d0!rhocent/1d15, '[*10^15 g/cm^3]'
      write(9,23) '* Central energy density =',epscent/1d15,
     &     '[*10^15 g/cm^3]'
      write(9,23) '* Central pressure =',P0/gc2*1d-10*cc*cc/1d35, 
     &     '[*10^35 g/cm/s^2]'

      if(switch) then
         Y(1) = DSQRT(1.5d0*h1/PI)/rho0 ! Newtonian Radius
      else
         Y(1) = DSQRT((1.5d0*h1/PI)/((eps0+P0)*(eps0+3.d0*P0))) ! TOV Radius
      endif
      Y(2) = 4d0/3d0*PI*rho0*Y(1)**3      ! Newtonian mass(r)
      Y(3) = nu0                          ! nu(r)
      Y(4) = 4d0/3d0*PI*eps0*Y(1)**3      ! TOV mass(r)
      Y(5) = Phi0                         ! TOV Phi(r)
      Y(6) = 0d0                          ! TOV Omega(r)
      Y(7) = 0d0                          ! Scalar quadrupole moment(r)
      Y(8) = Y(2)                         ! Baryonic mass(r)
c The numerical error does not diminish if one improves the boundary conditions
c for Y(6) ed Y(7); this happens because they go behaviours r0**3,4,5
c      Y(6) = -8d0*PI**2*rho0**2*Y(1)**5/3d0
c      Y(6) = P0*Pi*Y(1)**4
c      Y(7) = h1*Y(1)**3/rho0
      Y(9) = Y(1)**2*(1d0-2d0*PI/7d0*(eps0/3d0+11d0*P0+
     &     (eps0+P0)*depsdp0)*Y(1)**2)    ! (l=2,m=0) perturbation (r)
      Y(10) = 2d0*Y(1)*(1d0-4d0*PI/7d0*(eps0/3d0+11d0*P0+
     &     (eps0+P0)*depsdp0)*Y(1)**2)    ! dy(9)/dr
      call odeint(y,10,x1,x2,hh,nok,nbad,derivs_pseudotov)

      write(9,*) '*'
      write(9,*) '* PSEUDO-GR TERMS:'
      write(9,22) '* Pseudo-relativistic self-gravity potential =', Y(6)
      if((enne.ne.0d0).and.(kn.ne.0d0)) then
         write(9,22) '* Polytropic Newtonian self-gravity potential =',
     &        -3d0/(5d0-enne)*Y(2)**2/Y(1)
      endif
      write(9,22) '* Scalar quadrupole moment =',
     &     Y(7)
      if((enne.ne.0d0).and.(kn.ne.0d0)) then
         write(9,22) '* Polytropic Newtonian scalar quadrupole moment ='
     &        , kn/5d0*Y(1)**2*Y(2)
      endif

      if((enne.ne.0d0).and.(kn.ne.0d0)) then
         write(9,*) '*'
         write(9,*) '* Baryonic Rest Mass/R_poly =', Y(8)/dsqrt(K**enne)
         if(switch) then
            write(9,*) '* Newt Mass/R_poly =', Y(2)/dsqrt(K**enne)
         else
            write(9,*) '* Grav Mass/R_poly =', Y(4)/dsqrt(K**enne)
         endif
         write(9,*) '* Radius/R_poly =', Y(1)/dsqrt(K**enne)
      endif

      if(switch) then
         write(12,21) Y(2) ! Use Newtonian mass in Newtonian gravity
      else
         write(12,21) Y(4) ! Use TOV mass in GR
      endif
      write(12,21) Y(1)
      write(12,21) Y(6)
      write(12,21) Y(7)
      write(12,21) kn
      write(12,21) enne
      write(12,21) K

c**********************************************************************
c Love number calculation                                             *
c**********************************************************************
      call eos(DEXP(x2),rho0,eps0,depsdp0)
      comp = Y(4)/Y(1)
      yy = Y(1)*Y(10)/Y(9) !- 4d0*PI*Y(1)**3/Y(4)*eps0 ! Extra SQM term
      k2 = 8d0/5d0*comp**5*(1d0-2d0*comp)**2
     &     *(2d0+2d0*comp*(yy-1d0)-yy)/(2d0*comp*(6d0-3d0*yy
     &     +3d0*comp*(5d0*yy-8d0))+4d0*comp**3*(13d0-11d0*yy
     &     +comp*(3d0*yy-2d0)+2d0*comp**2*(1d0+yy))+3d0*(1d0
     &     -2d0*comp)**2*(2d0-yy+2d0*comp*(yy-1d0))*dlog(1d0-2d0*comp))
      write(9,*) '*'
      write(9,*) '* LOVE NUMBER:'
      write(9,*) '* k2 =', k2
      write(9,*) '* lambda =', 2d0/3d0*k2*(Y(1)*1d5)**5/(gc2*cc*cc),
     &     ' [g*cm^2*s^2]'
      write(*,24) Y(4)/MSun, Y(8)/Msun, Y(4)/Y(1)!p0*(K**enne)!/(gc2*1.d10/cc/cc)
!     &     2d0/3d0*k2*(Y(1)*1d5)**5/(gc2*cc*cc)/1d36

c**********************************************************************
c Generate P(r), rho(r), eps(r), rho_baryonic(r) profiles             *
c**********************************************************************
      call eos(p0,rho0,eps0,depsdp0)
c Boundary conditions
      if(switch) then
         x1 = DSQRT(1.5d0*h1/PI)/rho0 ! r0
         x2 = Y(1)                    ! Radius
         Z(1) = dlog(p0-h1)           ! ln(P(r0))
         Z(2) = 4d0/3d0*PI*rho0*x1**3 ! Mass(r)
      else
         x1 = DSQRT((1.5d0*h1/PI)/((eps0+P0)*(eps0+3.d0*P0))) ! r0
         x2 = Y(1)                                            ! Radius
         Z(1) = dlog(p0-h1)                                   ! ln(P(r0))
         Z(2) = 4d0/3d0*PI*eps0*x1**3                         ! Mass(r)
      endif
      dx = (x2-x1)/n_eos
      hh = 1.d-2*dx

      do j=1,n_eos
         x2 = x1 + dx
         call odeint(Z,2,x1,x2,hh,nok,nbad,derivs_pseudotov_r)  
         p0 = dexp(Z(1))
         call eos(p0,rho0,eps0,depsdp0)
         write(13,24) x2, p0, rho0, eps0, rho0/dsqrt(1d0-2d0*Z(2)/x2)
         x1 = x2
      enddo

      write(9,*) '*'
      write(9,*) '* P, RHO AND EPS PROFILES ARE IN profiles.dat'
      write(9,*) '*'
      write(9,*) '*'
      write(9,*) '*'

      close(9)
      close(12)
      close(13)

 222  STOP
      END


ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c-----------------------------------------
      SUBROUTINE DERIVS_PSEUDOTOV(X,Y,DYDX)
c-----------------------------------------
c Newtonian idrostatic equilibrium plus
c TOV equation for TOV mass, nu and Phi.
c-----------------------------------------
      implicit none
      real*8 X,Y(10),DYDX(10),RHO,PRE,EPS,DEPSDP,PI,drdp
      logical switch, mswitch

      common /NewtGR/ switch, mswitch

      PI=dacos(-1d0)

      PRE = DEXP(X) ! Integration variable is log(P)
c Y(1) = r
c Y(2) = NEWT mass
c Y(3) = TOV nu (g00 = exp(nu))
c Y(4) = TOV mass
c Y(5) = PseudoGR Phi
c Y(6) = PseudoGR Omega
c Y(7) = Scalar quadrupole moment
c Y(8) = Baryonic mass
c Y(9) = (l=2,m=0) Perturbation
c Y(10) = derivative of Y(9) wrt to r

      CALL EOS(PRE,RHO,EPS,DEPSDP)

      if(switch) then
         drdp = -Y(1)**2/Y(2)/RHO
      else
         drdp =-Y(1)*(Y(1)-2d0*Y(4))/(eps+pre)/(Y(4)+4d0*PI*Y(1)**3*pre)
      endif
      DYDX(1) = PRE*drdp
      DYDX(2) = 4.d0*PI*RHO*Y(1)**2*PRE*drdp
      DYDX(3) = -2d0*PRE/(EPS+PRE)
      DYDX(4) = 4d0*PI*Y(1)**2*EPS*PRE*drdp
      DYDX(5) = -PRE/RHO
      DYDX(6) = PRE*4d0*PI*Y(1)**3
      if(mswitch) then
         DYDX(7) = -4d0/3d0*PI*PRE*Y(1)**6/Y(2) ! Using Y(2) in GR too ***
      else
         DYDX(7) = -Y(1)**6/3d0/(Y(4)/4d0/PI/PRE+Y(1)**3)
      endif
c This r**6 requires to pass hh = 1d-8(x2-x1) to odeint to avoid rqks underflow
      DYDX(8) = 4.d0*PI*RHO*Y(1)**2*PRE*drdp/sqrt(1d0-2d0*Y(4)/Y(1))
      DYDX(9) = Y(10)*PRE*drdp
      DYDX(10) = (2d0/(1d0-2d0*Y(4)/Y(1))*Y(9)*
     &     (-2d0*PI*(5d0*EPS+9d0*PRE+DEPSDP*(EPS+PRE))
     &     +3d0/Y(1)**2+2d0/(1d0-2d0*Y(4)/Y(1))*(Y(4)/Y(1)**2
     &     +4d0*PI*Y(1)*PRE)**2)
     &     +2d0*Y(10)/Y(1)/(1d0-2d0*Y(4)/Y(1))*
     &     (-1d0+Y(4)/Y(1)+2d0*PI*Y(1)**2*(EPS-PRE)))
     &     *PRE*drdp

      RETURN
      END


c------------------------------------------------------------------------------
      subroutine derivs_pseudotov_r(x,y,dydx)
c------------------------------------------------------------------------------
c   x=r   y(1)=ln(p)  y(2)=mass
c------------------------------------------------------------------------------
      implicit none
      real*8 x,y(2),dydx(2)
      real*8 r,rho,pre,eps,pi,dpdr,dmdr,depsdp
      logical switch, mswitch

      common /NewtGR/ switch, mswitch

      pi = dacos(-1.d0)
      pre = dexp(y(1))

      call eos(pre,rho,eps,depsdp)

      if(switch) then
c Uses Newtonian mass (notice rho)
         dpdr = -rho*y(2)/x/x
         dmdr = 4.d0*pi*rho*x**2
      else
c Uses TOV mass (notice eps)
         dpdr = -(y(2)+4d0*pi*x**3*pre)*(eps+pre)/(x*(x-2d0*y(2)))
         dmdr = 4.d0*pi*eps*x**2
      endif
      
      dydx(1) = dpdr/pre
      dydx(2) = dmdr 

      return
      end

c------------------------------------------------------------------------------
      subroutine kappa(enne,kn)
c------------------------------------------------------------------------------
      implicit none
      real*8 enne, kn

      if(enne.eq.0d0) then   ! To be used if the EOS is tabulated
         kn = 0d0
      else if(enne.eq.0.5d0) then
         kn = 0.81482d0
      else if(enne.eq.4d0/7d0) then
         kn = 0.79046d0
      else if(enne.eq.1.5d0) then
         kn = 0.51149d0
      else
         enne = 1d0          ! n=1 is the default case
         kn = 0.65345d0
      endif

      return
      end

c------------------------------------------------------------------------------
      include 'odeint_star.f'
      include 'lintrp.f'
      include 'locate.f'
      include 'eos.f'
c------------------------------------------------------------------------------

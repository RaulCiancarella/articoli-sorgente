c---------------------------------------------------------------------
      SUBROUTINE EOS(pres,rho,epsilon,depsdp)
c---------------------------------------------------------------------
c The input and the output are in natural units (Km^-2, Km^-2, Km^-2);
c the first time it is executed, it reads an EOS table in cgs,
c in the form eps p (eps is the mass-energy density).
c The commented commands are for calculated derivatives that here are
c not necessary.
c---------------------------------------------------------------------
      implicit none
      integer i, j, n, nn, np
      parameter (nn=2000) 
      parameter (n= 176 )
      real*8 lpres(nn),leps(nn),lrho(nn),dlp,ldepsdp(nn),lder,cs2
      real*8 p(n),eps(n),mdens(n)!total energy density, pressure, mass density
      real*8 pres0,rho0,dens0,pres,epsilon,rho,depsdp,plog,elog,rholog
      real*8 x(n),y(n),z(n),facteps,factp,gc2,cc,GG,A0,A1,A2,A3
      parameter (gc2=7.42365d-29)
      parameter (GG=6.673d-8)
      parameter (cc=2.998d10)
      logical flag
      data flag/.true./
c dln(P), ln(P), ln(rho), ln(eps), ln(deps/dp)
      save dlp,lpres,lrho,leps,ldepsdp

      facteps=gc2*1.d10      ! 1/km^2 <--> g/cm^3
      factp=gc2*1.d10/cc/cc  ! 1/km^2 <--> g/s^2/cm

c-----------------------------------------------------------
c This is done only the first time the subroutine is called
c-----------------------------------------------------------
      IF (FLAG) THEN
c Read the data file from the desired nuclear EOS
      open(unit=1,file='EOS_TABLES/sqm3.eos')
         do i=1,n
            read(1,*) eps(i), p(i), mdens(i)
            x(i)=dlog(p(i)*factp) 
            y(i)=dlog(eps(i)*facteps)
            z(i)=dlog(mdens(i)*facteps)
         enddo
         close(1)
         
         dlp = (x(n)-x(1))/dble(nn-1)

c Build nn-dimensional array equally spaced in ln(P) and
c an nn-dimensional array with ln(eps(ln(P))).
         open(11,file='eos.dat')
         do j=1,nn
            lpres(j) = x(1) + (j-1)*dlp
            pres0 = dexp(lpres(j))
            call lintrp(dens0,pres0,y,x,n,2)
            call lintrp(rho0,pres0,z,x,n,2)
            leps(j) = dlog(dens0)
            lrho(j) = dlog(rho0)
            write(11,*) pres0/factp, rho0/facteps, dens0/facteps
         enddo

c Build the ln(deps/dp) array and prepare the P, rho, eps file
cc         A0 = -1.d0/3.d0
cc         A1 = -1.d0/2.d0
cc         A2 = 1.d0
cc         A3 = -1.d0/6.d0
         do j=2,nn-2
            ldepsdp(j) = (leps(j+1) - leps(j-1))/2.d0/dlp
cc            ldepsdp(j) = (A0*leps(j-1) + A1*leps(j) + A2*leps(j+1) 
cc     &                +A3*leps(j+2))/dlp
            dens0 = dexp(leps(j))
            pres0 = dexp(lpres(j))
            rho0 = dexp(lrho(j))
c            write(11,*) pres0/factp, rho0/facteps, dens0/facteps
c     &           pres0/dens0/ldepsdp(j)*cc*cc
         enddo
         
         ldepsdp(nn-1) = (leps(nn) - leps(nn-2))/2.d0/dlp
         
         close(11)
         flag = .false.
      ENDIF

c--------------------------------------------------
c This is done every time the subroutine is called
c--------------------------------------------------
      plog = dlog(pres)
c      if (plog.lt.lpres(3)) print*,'pressure too small'
c      if (plog.gt.lpres(nn-1)) print*,'pressure too large'
c      do np=3,nn-1
c         if (plog.le.lpres(np)) go to 30
c      enddo
c 30   continue ! now our point is between np-1 and np
      
      call locate(lpres,nn,plog,np)
      np = np-1 ! now our point is between np-1 and np

      elog = leps(np-1)+(plog-lpres(np-1))*(leps(np)-leps(np-1))/dlp
      epsilon = dexp(elog)
      rholog = lrho(np-1)+(plog-lpres(np-1))*(lrho(np)-lrho(np-1))/dlp
      rho = dexp(rholog)
      lder = ldepsdp(np-1) + 
     &       (plog-lpres(np-1))*(ldepsdp(np)-ldepsdp(np-1))/dlp
      cs2 = pres/epsilon/lder
      depsdp=1.d0/cs2
c      dpdrho = cs2 !*** 

      return
      end

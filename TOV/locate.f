c------------------------------------------------------------------------------
c Given an array xx(1:n), and given a value x, returns a value j such that x is
c between xx(j) and xx(j+1). xx(1:n) must be monotonic, either increasing or
c decreasing. j=0 or j=n is returned to indicate that x is out of range.

      SUBROUTINE locate(xx,n,x,j)
      INTEGER j,n
      DOUBLE PRECISION x,xx(n)
      INTEGER jl,jm,ju
      jl=0
      ju=n+1
10    if(ju-jl.gt.1)then
        jm=(ju+jl)/2
        if((xx(n).gt.xx(1)).eqv.(x.gt.xx(jm)))then
          jl=jm
        else
          ju=jm
        endif
      goto 10
      endif
      j=jl
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software *!^3]87J,.
